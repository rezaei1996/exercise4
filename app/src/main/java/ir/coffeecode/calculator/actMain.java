package ir.coffeecode.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class actMain extends AppCompatActivity {

    //Memory
    float fltMemory = 0;
    //Status Dot Button
    boolean bitDot = true;
    //Status Answer
    boolean bitAnswer = false;
    //Operand
    float numA = 0;
    float numB = 0;
    //Operations
    String strOpt = "";
    //Input Value
    String strBtnNum = "";
    String strTxvResult = "";
    String strTxvDisplay = "";
    //Views Variables
    TextView txvResult;
    TextView txvDisplay;
    //Result Variable
    float fltResult = 0;
    int intResult = 0;
    float fltCmp = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_main);

        txvResult = (TextView) findViewById(R.id.txvResult);
        txvDisplay = (TextView) findViewById(R.id.txvDisplay);
    }

    public void onClick_btnNum(View view) {
        Button btnOperand = (Button) findViewById(view.getId());
        strBtnNum = btnOperand.getText().toString();
        strTxvResult = txvResult.getText().toString();

        if (!bitAnswer) {
            if (!strBtnNum.equals("."))
                if (!strTxvResult.equals("0")) {
                    txvResult.append(strBtnNum);
                } else {
                    txvResult.setText(strBtnNum);
                }
            else if (bitDot) {
                txvResult.append(strBtnNum);
                bitDot = false;
            }
        } else {
            bitAnswer = false;
            txvResult.setText("0");
            strTxvResult = "0";
            if (!strBtnNum.equals("."))
                if (!strTxvResult.equals("0")) {
                    txvResult.append(strBtnNum);
                } else {
                    txvResult.setText(strBtnNum);
                }
            else if (bitDot) {
                txvResult.append(strBtnNum);
                bitDot = false;
            }
        }
    }

    public void onClick_Operations(View view) {
        Button btnOperator = (Button) findViewById(view.getId());
        strTxvResult = txvResult.getText().toString();
        strOpt = btnOperator.getText().toString();
        txvResult.setText("0");
        bitDot = true;
        txvDisplay.setText(strTxvResult + " " + strOpt + " ");
        numA = Float.parseFloat(strTxvResult);
    }

    public void onClick_Equal(View view) {
        strTxvResult = txvResult.getText().toString();
        strTxvDisplay = txvDisplay.getText().toString();
        txvDisplay.append(strTxvResult);
        bitDot = true;
        bitAnswer = true;
        numB = Float.parseFloat(strTxvResult);
        switch (strOpt) {
            case "+":
                fltResult = numA + numB;
                break;
            case "-":
                fltResult = numA - numB;
                break;
            case "/":
                fltResult = numA / numB;
                break;
            case "*":
                fltResult = numA * numB;
                break;
        }
        intResult = (int) fltResult;
        fltCmp = fltResult % intResult;
        if (fltCmp == 0.0) {
            txvResult.setText(String.valueOf(intResult));
        } else {
            txvResult.setText(String.valueOf(fltResult));
        }
    }

    public void onClick_CE(View view) {
        txvResult.setText("0");
        bitDot = true;
    }

    public void onClick_Clear(View view) {
        txvDisplay.setText("");
        txvResult.setText("0");
        bitDot = true;
    }

    public void onClick_MS(View view) {
        fltMemory = Float.parseFloat(txvResult.getText().toString());
    }

    public void onClick_MR(View view) {
        txvResult.setText(String.valueOf(fltMemory));
    }

    public void onClick_MAdd(View view) {
        fltMemory += Float.parseFloat(txvResult.getText().toString());
    }

    public void onClick_MNegative(View view) {
        fltMemory -= Float.parseFloat(txvResult.getText().toString());
    }

    public void onClick_MC(View view) {
        fltMemory = 0;
    }
}
